# Servidor de APIs

Servidor de APIs com Json-Server

# Para rodar o projeto
0. Instalar no ambiente de desenvolvimento o Node.js (https://nodejs.org).
1. Fazer o checkout do projeto utilizando: git clone https://gitlab.com/gilbriatore/2021/web/srv-apis.git
2. Rodar, na pasta do servidor, o comando: npm install
3. Executar, na pasta do servidor, o comando: npm start
4. O servidor será iniciado em `http://localhost:8000/`. 